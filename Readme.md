This program finds the daily pizza toppings from Arizmendi Bakery and prints
them out on standard output.

### Usage

    $ ./arizmendi
    marinated artichoke hearts, mixed greens, smoked gouda, rosemary oil, p&p

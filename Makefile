# Usage:
#
#     $ make         # build a binary for the current architecture
#     $ make linux   # build a Linux binary
#     $ make clean   # clean up temporary files
#     $ make clobber # remove all artifacts
#

SOURCE_FILES := $(shell find . -type f -name "*.go")

# make
# The first task defined is the default task
# The default task builds a binary for the current architecture
build/arizmendi: build
	go build -o $@

# make linux
# The linux task builds a binary for Linux architecture
.PHONY: linux
linux: build/linux/arizmendi

# make clobber
# The clobber task removes all artifacts
.PHONY: clobber
clobber: clean
	rm -rf build

# make clean
# The clean task cleans up the repository
.PHONY: clean
clean:
	rm -rf build/arizmendi
	rm -rf build/linux/arizmendi

build:
	mkdir -p $@

build/linux/arizmendi: build/linux $(SOURCE_FILES)
	GOOS=linux go build -o $@

build/linux:
	mkdir -p $@

package main

import (
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/PuerkitoBio/goquery"
)

func main() {
	body := requestPizzaCalendar()
	toppings := extractToppings(body)
	body.Close()

	if toppings == "" {
		log.Fatal("No toppings, Arizmendi is closed on Mondays.")
	}

	fmt.Printf("%s\n", toppings)
}

func requestPizzaCalendar() io.ReadCloser {
	res, err := http.Get("https://www.arizmendibakery.com/pizza")

	if err != nil {
		log.Print("Unable to connect to arizmendibakery.com")
		log.Fatal(err)
	}
	if res.StatusCode != 200 {
		log.Print("Invalid HTTP status code %d", res.StatusCode)
		log.Fatal(err)
	}

	return res.Body
}

func extractToppings(r io.Reader) string {
	var toppings string

	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		log.Fatal(err)
	}

	doc.Find(".active").Each(func(i int, s *goquery.Selection) {
		toppings = toppings + s.Find(".yasp-item").Text()
	})

	return toppings
}

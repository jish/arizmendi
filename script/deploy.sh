#!/usr/bin/env bash

if [[ -z $SERVER ]]; then
  echo "$0: error: Please set the SERVER environment variable"
  exit 1
fi

make linux
scp build/linux/arizmendi $SERVER:bin/arizmendi
